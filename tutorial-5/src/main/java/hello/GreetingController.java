package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        //model.addAttribute("name", name);
        if (name == null || name.equals("")){
            model.addAttribute("title", "This is my CV");
        }
        else{
            model.addAttribute("title",name +", I hope you interested to hire me");
        }
        StringBuilder cv = new StringBuilder();
        cv.append("Kezia Irene\n");
        cv.append("Birthdate: October 15th 1998\n");
        cv.append("Birthplace: Jakarta\n");
        cv.append("Education history:\n");
        cv.append(" - Universitas Indonesia (2016-present)\n");
        cv.append(" - SMAK 3 Penabur Jakarta (2013-2016)\n");
        cv.append(" - SMPK 2 Penabur Jakarta (2010-2013)\n");
        cv.append(" - SDK 1 Penabur Jakarta (2004-2010)\n");
        cv.append(" - TKK 1 Penabur Jakarta (2001-2003)\n");
        model.addAttribute("cv", cv.toString());


        StringBuilder description = new StringBuilder();
        description.append("I have been studying Computer Science at University of Indonesia since September 2016. \n");
        description.append("I really enjoy music, I have been studying piano, violin, flute, musical composition, " +
                "and film scoring since 2005. \n");
        description.append("I also have been developing skill at business plan and business case. \n");

        model.addAttribute("description", description.toString());

        return "greeting";
    }

}
