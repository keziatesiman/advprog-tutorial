package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
    	if(salary < 100000.00){
    		throw new IllegalArgumentException();
    	}


    	this.name = name;
    	this.salary = salary;
  
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return salary;
    }
    
    @Override
    public String getName() {
        //TODO Implement
    	return name;
    }
    
    @Override
    public String getRole() {
        //TODO Implement
    	return "CTO";
    }
}

