package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner   extends Employees {

	public UiUxDesigner  (String name, double salary) {
        //TODO Implement
		if(salary < 90000.00){
    		throw new IllegalArgumentException();
    	}


    	this.name = name;
    	this.salary = salary;
  
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return salary;
    }
    
    @Override
    public String getName() {
        //TODO Implement
    	return name;
    }
    
    @Override
    public String getRole() {
        //TODO Implement
    	return "UI/UX Designer";
    }
	
}
