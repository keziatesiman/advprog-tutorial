package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class BlackPepperSauce implements Sauce {
    public String toString() {
        return "Black Pepper Sauce";
    }
}
