package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
	private static Singleton uniqueInstance;
	 
	private Singleton() {}
 
	public static Singleton getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new Singleton();
		}
		return uniqueInstance;
	}
 
	// other useful methods here
	public String getDescription() {
		return "I'm a classic Singleton!";
	}

    // TODO Implement me!
    // What's missing in this Singleton declaration?
}
