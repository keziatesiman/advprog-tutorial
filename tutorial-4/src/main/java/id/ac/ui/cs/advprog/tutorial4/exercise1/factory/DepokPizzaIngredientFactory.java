package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new MediumCrustDough();
    }

    public Sauce createSauce() {
        return new BlackPepperSauce();
    }

    public Cheese createCheese() {
        return new BrieCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Carrot(), new Spinach(), new Garlic(), new Mushroom()};
        return veggies;
    }

    public Clams createClam() {
        return new HotClams();
    }
}
