package id.ac.ui.cs.advprog.tutorial4.exercise1;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class DepokPizzaStoreTest {
	protected PizzaStore pizzaStore;
	
	@Before
	public void setUp() {
		pizzaStore = new DepokPizzaStore();
	}
	
	@Test
	public void testOrderPizza() {
		Pizza pizza;
		pizza = pizzaStore.orderPizza("cheese");
		assertTrue(pizza instanceof CheesePizza);
		String expected = "---- Depok Style Cheese Pizza ----\n" + 
				"MediumCrust style extra medium crust dough\n" + 
				"Black Pepper Sauce\n" + 
				"Shredded Brie\n"; // put the expected value here
        assertEquals(expected, pizza.toString());

		pizza = pizzaStore.orderPizza("veggie");
		assertTrue(pizza instanceof VeggiePizza);
		String expected1 = "---- Depok Style Veggie Pizza ----\n" + 
				"MediumCrust style extra medium crust dough\n" + 
				"Black Pepper Sauce\n" + 
				"Shredded Brie\n" + 
				"Carrot, Spinach, Garlic, Mushrooms\n"; // put the expected value here
        assertEquals(expected1, pizza.toString());

		pizza = pizzaStore.orderPizza("clam");
		assertTrue(pizza instanceof ClamPizza);
		String expected2 = "---- Depok Style Clam Pizza ----\n" + 
				"MediumCrust style extra medium crust dough\n" + 
				"Black Pepper Sauce\n" + 
				"Shredded Brie\n" + 
				"Hot Clams from Honolulu Hawaii\n"; // put the expected value here
        assertEquals(expected2, pizza.toString());

	}
}
