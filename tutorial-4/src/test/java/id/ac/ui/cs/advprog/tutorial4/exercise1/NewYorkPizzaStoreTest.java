package id.ac.ui.cs.advprog.tutorial4.exercise1;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class NewYorkPizzaStoreTest {
	protected PizzaStore pizzaStore;
	
	@Before
	public void setUp() {
		pizzaStore = new NewYorkPizzaStore();
	}
	
	@Test
	public void testOrderPizza() {
		Pizza pizza;
		pizza = pizzaStore.orderPizza("cheese");
		assertTrue(pizza instanceof CheesePizza);
		String expected = "---- New York Style Cheese Pizza ----\n" + 
				"Thin Crust Dough\n" + 
				"Marinara Sauce\n" + 
				"Reggiano Cheese\n"; // put the expected value here
        assertEquals(expected, pizza.toString());

		
		pizza = pizzaStore.orderPizza("veggie");
		assertTrue(pizza instanceof VeggiePizza);
		String expected1 = "---- New York Style Veggie Pizza ----\n" + 
				"Thin Crust Dough\n" + 
				"Marinara Sauce\n" + 
				"Reggiano Cheese\n" + 
				"Garlic, Onion, Mushrooms, Red Pepper\n"; // put the expected value here
        assertEquals(expected1, pizza.toString());

		
		pizza = pizzaStore.orderPizza("clam");
		assertTrue(pizza instanceof ClamPizza);
		String expected2 = "---- New York Style Clam Pizza ----\n" + 
				"Thin Crust Dough\n" + 
				"Marinara Sauce\n" + 
				"Reggiano Cheese\n" + 
				"Fresh Clams from Long Island Sound\n"; // put the expected value here
        assertEquals(expected2, pizza.toString());

	}
}
