import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

	Movie movie;
    Movie movie2;


    @Before
    public void setUp() {
        movie = new Movie("Piglet's Big Movie", Movie.REGULAR);
        movie2 = new Movie("Winnie the Pooh",Movie.NEW_RELEASE);
    }

    @Test
    public void getTitle() {
    	assertEquals("Piglet's Big Movie", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Springtime with Roo");

        assertEquals("Springtime with Roo", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
    	assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {        
    	movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }
    @Test
    public void testEquals() {
        assertTrue(movie.equals(movie));
        assertFalse(movie.equals(movie2));
        assertFalse(movie.equals(null));
        assertTrue(movie.equals(new Movie("Piglet's Big Movie", Movie.REGULAR)));
    }


    
    
}