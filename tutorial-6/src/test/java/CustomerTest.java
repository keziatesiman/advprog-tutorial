import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

	Customer customer;
	Movie movie1;
	Movie movie2;
	Movie movie3;
	Rental rent1;
	Rental rent2;
	Rental rent3;
	
	@Before
    public void setUp() {
        customer = new Customer("Kezia");
        movie1 = new Movie("Piglet's Big Movie", Movie.REGULAR);
        movie2 = new Movie("Winnie the Pooh", Movie.CHILDREN);
        movie3 = new Movie("Christopher Robin", Movie.NEW_RELEASE);
        rent1 = new Rental(movie1, 3);
        rent2 = new Rental(movie2, 4);
        rent3 = new Rental(movie3, 3);
    }

    @Test
    public void getName() {
        assertEquals("Kezia", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    public void statementWithMultipleMovies() {
        // TODO Implement me!
    	customer.addRental(rent2);
    	customer.addRental(rent3);
    	String result = customer.statement();
    	String[] lines = result.split("\n");
    	
    	assertEquals(5, lines.length);
        assertTrue(result.contains("Amount owed is 12.0"));
        assertTrue(result.contains("3 frequent renter points"));

    }
    
    @Test
    public void htmlStatement() {
        customer.addRental(rent1);
        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<P>You owe <EM>3.5</EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM>"
                + "1</EM> frequent renter points<P>"));
    }

    
    
}