package applicant;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
	
	private static Applicant applicant;
	
	@Before
	public void setUp() {
		applicant = new Applicant();
	}
	
	@Test
	public void testCreditQualifiedApplicant() {
		Predicate<Applicant> creditCheck = 
		          theApplicant -> theApplicant.getCreditScore() > 600; 
        String result = applicant.evaluate(applicant, creditCheck);    
        
        assertEquals("accepted", result);

	}
	
	@Test
	public void testEmployementQualifiedApplicant() {
		Predicate<Applicant> creditCheck = theApplicant -> theApplicant.getCreditScore() > 600; 
		Predicate<Applicant> employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
		String result2 = applicant.evaluate(applicant, creditCheck.and(employmentCheck));
        
        assertEquals("accepted", result2);

	}
	
	@Test
	public void testCrimeQualifiedApplicant() {
		Predicate<Applicant> employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
		Predicate<Applicant> crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();
		String result3 = applicant.evaluate(applicant, crimeCheck.and(employmentCheck));
        
        assertEquals("rejected", result3);

	}
	
	@Test
	public void testAllQualifiedApplicant() {
		Predicate<Applicant> creditCheck = theApplicant -> theApplicant.getCreditScore() > 600; 
		Predicate<Applicant> employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
		Predicate<Applicant> crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();
		String result4 = applicant.evaluate(applicant, crimeCheck.and(creditCheck).and(employmentCheck));
        
        assertEquals("rejected", result4);

	}

}
